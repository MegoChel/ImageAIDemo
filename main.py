# This is a sample Python script.
from imageai.Detection import VideoObjectDetection
import cv2


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def detectOnVideo():
    video_src = 'mazda.mp4'
    camera = cv2.VideoCapture(video_src)

    detector = VideoObjectDetection()
    detector.setModelTypeAsYOLOv3()
    detector.setModelPath("yolov3.pt")
    detector.loadModel()

    video_path = detector.detectObjectsFromVideo(input_file_path=video_src,
                                                 output_file_path="output"
                                                 , frames_per_second=20, log_progress=True)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    detectOnVideo()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
